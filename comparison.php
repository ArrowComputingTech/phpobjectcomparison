<?php

  class Posts {
    public $post;

    function compareEqual($obj1, $obj2) {
      if($obj1 === $obj2) {
        return true;
      } else {
        return false;
      }
    }

    function compareIdentical($obj1, $obj2) {
      if($obj1 == $obj2) {
        return true;
      } else {
        return false;
      }
    }

  } 

  $post1 = new posts("first post");
  $post2 = new posts("first post");

  $p = new Posts();

  echo $p->compareEqual($post1, $post2) ? "SAME" : "DIFFERENT";
  echo " -------" . PHP_EOL . PHP_EOL;;

  $post3 = $post1;
  echo $p->compareEqual($post1, $post3) ? "SAME" : "DIFFERENT";
  echo " ++++++++" . PHP_EOL . PHP_EOL;

  $post4 = clone($post1);
  echo $p->compareEqual($post1, $post4) ? "SAME" : "DIFFERENT";
  echo " //////////" . PHP_EOL . PHP_EOL;

  echo $p->compareIdentical($post1, $post2) ? "SAME" : "DIFFERENT";
  echo PHP_EOL . PHP_EOL;;

